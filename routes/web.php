<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('login','LoginController@index');
Route::post('login-new','LoginController@store')->name('pasok');
Route::get('logout','LoginController@logout')->name('gawas');

Route::resource('branches','BranchController');
Route::get('/branches/delete/{id}','BranchController@delete')->name('branches.delete'); 

Route::resource('employees','EmployeeController');
Route::get('/employees/delete/{id}','EmployeeController@delete')->name('employees.delete');

Route::resource('customers','CustomerController');
Route::get('/customers/delete/{id}','CustomerController@delete')->name('customers.delete');

Route::resource('products','ProductController');
Route::get('/products/delete/{id}','ProductController@delete')->name('products.delete');

Route::resource('sales','SalesController');
Route::get('/sales/delete/{id}','SalesController@delete')->name('sales.delete');

Route::resource('/users','UserController');