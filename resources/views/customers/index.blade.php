@extends('layouts.app')
@section('content')
<h1 class="page title">Customer Module</h1>
<div class="row">
    <div class="col-12">
        <a href="{{ route('customers.create') }}" class="btn btn-primary mb-1">Create Entry</a>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="text-center">Name</th>
                    <th class="text-center">Branch</th>
                    <th class="text-center">Agent</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($customers as $data)
                <tr>
                    <td>{{ $data->name}}</td>
                    <td>{{ @$data->branch->name}}</td>
                    <td>{{ $data->agent}}</td>
                    <td class="text-center">
                        <a href="{{ action('CustomerController@edit',$data->id) }}" class="btn btn-success">Edit</a> 
                        <a href="{{ action('CustomerController@delete',$data->id) }}" class="btn btn-danger">Delete </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {!! $customers->links() !!}
    </div>
</div>
@endsection