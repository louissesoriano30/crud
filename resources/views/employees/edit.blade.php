@extends('layouts.app')
@section('content')
<h1 class="page title">Employee Module - Edit {{ $employee->name}}</h1>
<div class="row">
    <div class="col-12">
        <a href="{{ route('employees.index') }}" class="btn btn-primary mb-1">Back to Index</a>
        {!! Form::model($employee,['method'=>'PATCH','action'=>['EmployeeController@update',$employee->id]]) !!}
        @include('alert')
        <div class="card mt-3">
            <div class="card-header">Edit Employee</div>
            <div class="card-body">
                <div class="mb-3">
                    {!! Form::label('Enter Name') !!}
                    {!! Form::text('name',null,['class'=>'form-control']) !!}
            </div>
            <div class="mb-3">
                {!! Form::label('Select Branch') !!}
                {!! Form::select('branch_id',$branches,null,['class'=>'form-control','placeholder'=>'--Select One--']) !!}
        </div>
        <div class="mb-3">
            {!! Form::label('Enter Type') !!}
            {!! Form::text('type',null,['class'=>'form-control']) !!}
    </div>
            <div class="card-footer">
                {!! Form::submit('Save Entry',['class'=>'btn btn-primary']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection