@extends('layouts.app')
@section('content')
<h1 class="page title">Sales Module</h1>
<div class="row">
    <div class="col-12">
        <a href="{{ route('sales.create') }}" class="btn btn-primary mb-1">Create Entry</a>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="text-center">Product Name</th>
                    <th class="text-center">Price</th>
                    <th class="text-center">Quantity</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($sales as $data)
                <tr>
                    <td>{{ $data->name}}</td>
                    <td>{{ $data->prices}}</td>
                    <td>{{ $data->quantity}}</td>
                    <td class="text-center">
                        <a href="{{ action('SalesController@edit',$data->id) }}" class="btn btn-success">Edit</a> 
                        <a href="{{ action('SalesController@delete',$data->id) }}" class="btn btn-danger">Delete </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {!! $sales->links() !!}
    </div>
</div>
@endsection