@extends('layouts.app')
@section('content')
<h1 class="page title">Sales Module - Edit{{ $sales->name}}</h1>
<div class="row">
    <div class="col-12">
        <a href="{{ route('sales.index') }}" class="btn btn-primary mb-1">Back to Index</a>
        {!! Form::open(['method'=>'PATCH','action'=>'SalesController@store',$sales->id]) !!}
        @include('alert')
        <div class="card mt-3">
            <div class="card-header">Edit Sales</div>
            <div class="card-body">
                <div class="mb-3">
                    {!! Form::label('Product Name') !!}
                    {!! Form::text('name',null,['class'=>'form-control']) !!}
                <div class="mb-3">
                    {!! Form::label('Price') !!}
                    {!! Form::text('prices',null,['class'=>'form-control']) !!}
                </div>
                       <div class="mb-3">
                          {!! Form::label('Quantity') !!}
                         {!! Form::text('quantity',null,['class'=>'form-control']) !!}
                       </div>
                        <div class="mb-3">
                          {!! Form::label('Amount') !!}
                          {!! Form::text('amount',null,['class'=>'form-control']) !!}
                        </div>
            </div>
            <div class="card-footer">
                {!! Form::submit('Update Entry',['class'=>'btn btn-primary']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection