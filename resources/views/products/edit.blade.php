@extends('layouts.app')
@section('content')
<h1 class="page title">Product Module - Edit{{ $product->name }}</h1>
<div class="row">
    <div class="col-12">
        <a href="{{ route('products.index') }}" class="btn btn-primary mb-1">Back to Index</a>
        {!! Form::open(['method'=>'PATCH','action'=>'ProductController@store',$product->id]) !!}
        @include('alert')
        <div class="card mt-3">
            <div class="card-header">Edit Product</div>
              <div class="card-body">
                    <div class="mb-3">
                      {!! Form::label('Product Name') !!}
                      {!! Form::text('name',null,['class'=>'form-control']) !!}
                    </div>
                    <div class="mb-3">
                      {!! Form::label('Product Code') !!}
                      {!! Form::text('codes',null,['class'=>'form-control']) !!}
                   </div>
                   <div class="mb-3">
                      {!! Form::label('Date Arrival') !!}
                      {!! Form::text('date arrival',null,['class'=>'form-control']) !!}
                    </div>
                    <div class="mb-3">
                        {!! Form::label('Expiry Date') !!}
                        {!! Form::text('expiry date',null,['class'=>'form-control']) !!}
                   </div>
                   <div class="mb-3">
                        {!! Form::label('Selling Price') !!}
                        {!! Form::text('selling price',null,['class'=>'form-control']) !!}
                   </div>
                   <div class="mb-3">
                        {!! Form::label('Original Price') !!}
                        {!! Form::text('original price',null,['class'=>'form-control']) !!}
                   </div>
                   <div class="mb-3">
                        {!! Form::label('Quantity') !!}
                        {!! Form::text('quantity',null,['class'=>'form-control']) !!}
                   </div>
                   <div class="card-footer">     
                      {!! Form::submit('Update Entry',['class'=>'btn btn-primary']) !!}
                    </div>
                </div>
            </div>                   
         </div>                     
     </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection