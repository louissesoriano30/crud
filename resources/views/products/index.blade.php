@extends('layouts.app')
@section('content')
<h1 class="page title">Product Module</h1>
<div class="row">
    <div class="col-12">
        <a href="{{ route('products.create') }}" class="btn btn-primary mb-1">Create Entry</a>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="text-center">Product Name</th>
                    <th class="text-center">Product Code</th>
                    <th class="text-center">Date Arrival</th>
                    <th class="text-center">Expiry Date</th>
                    <th class="text-center">Selling Price</th>
                    <th class="text-center">Original Price</th>
                    <th class="text-center">Quantity</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($products as $data)
                <tr>
                    <td>{{ $data->name}}</td>
                    <td>{{ $data->codes}}</td>
                    <td>{{ $data->datearrival}}</td>
                    <td>{{ $data->expirydate}}</td>
                    <td>{{ $data->sellingprice}}</td>
                    <td>{{ $data->originalprice}}</td>
                    <td>{{ $data->quantity}}</td>
                    <td class="text-center">
                        <a href="{{ action('ProductController@edit',$data->id) }}" class="btn btn-success">Edit</a> 
                        <a href="{{ action('ProductController@delete',$data->id) }}" class="btn btn-danger">Delete </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {!! $products->links() !!}
    </div>
</div>
@endsection