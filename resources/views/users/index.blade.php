@extends('layouts.app')
@section('content')
<h1 class="page title">Users Module</h1>
@include('alert')
<div class="row">
    <div class="col-12">
        <a href="#add" data-toggle="modal" class="btn btn-primary mb-1">Create Entry</a>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center">Username</th>
                    <th class="text-center">Email Address</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $data)
                <tr>
                    <td>{{ $data->id}}</td>
                    <td>{{ $data->name}}</td>
                    <td>{{ $data->email}}</td>
                    <td class="text-center">
                        <a href="#" class="btn btn-success">Edit</a> 
                        <a href="#" class="btn btn-danger">Delete </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {!! $users->links() !!}
    </div>
</div>
@endsection
@section('modals')
{!! Form::open(['method'=>'POST','action'=>'UserController@store']) !!}
<div class="modal fade" id="add" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Create New User</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
              <div class="col-md-12">
                 <div class="form-group">
                    {!! Form::label('Enter Username') !!}
                    {!! Form::text('name',null,['class'=>'form-control']) !!}
                 </div>
                 <div class="form-group">
                    {!! Form::label('Enter Email') !!}
                    {!! Form::text('email',null,['class'=>'form-control']) !!}
                 </div>
                 <div class="form-group">
                    {!! Form::label('Enter Password') !!}
                    {!! Form::password('password',['class'=>'form-control']) !!}
                 </div>
              </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>
  {!! Form::close() !!}
@endsection