<?php

namespace App\Http\Controllers;

use App\User;
use Request;
use Validator;

class UserController extends Controller
{
    
    public function index(){
        $users = User::paginate(10);
        return view('users.index',compact('users'));
    }

    public function store(){
        $validator = Validator::make(Request::all(), [
            'name'                =>    'required|unique:users',
            'email'               =>    'required|email|unique:users',
            'password'            =>    'required|min:6'
          ],
          [ 
           'name.required'              =>    'Username Required',
           'email.required'             =>    'Email Required',
           'password.required'          =>    'Password Required',
   
          ]);
   
         if ($validator->fails()) {
             return redirect()->back()
              ->withErrors($validator)
              ->withInput();
         }

         User::create([
            'name'          =>       Request::get('name'),
            'email'         =>       Request::get('email'),
            'password'      =>      \Hash::make(preg_replace('/\s+/', '',Request::get('password'))),
        ]);
        toastr()->success('Data has been saved successfully!');
        return redirect()->back();

    }

    public function update($id){

    }
}
