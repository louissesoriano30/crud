<?php

namespace App\Http\Controllers;

use App\Sales;
use Request;

class SalesController extends Controller
{
  
    public function index()
    {
        $sales= Sales::paginate(10);
        return view('sales.index',compact('sales'));

    }
    public function create()
    {
        return view('sales.create');
    }

    
    public function store()
    {
        $validator = Validator::make(Request::all(), [
            'name'                  =>    'required',
            'price'                 =>    'required',
            'quantity'              =>    'required',
            'amount'                =>    'required',
          ],
          [ 
           'name.required'             =>    'Product Name Required',
           'price.required'            =>    'Price Required',
           'quantity.required'         =>    'Quantity Required',
           'amount.required'           =>    'Amount Required',
   
          ]);
   
         if ($validator->fails()) {
             return redirect()->back()
              ->withErrors($validator)
              ->withInput();
         }
   
         Sales::create(Request::all());
          toastr()->success('Sales has been saved successfully!');
          return redirect()->back();
    }

   
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        $sales = Sales::find($id);
        return view('sales.edit',compact('sales'));
    }

    
    public function update($id)
    {
        $validator = Validator::make(Request::all(), [
            'name'                  =>    'required',
            'price'                 =>    'required',
            'quantity'              =>    'required',
            'amount'                =>    'required',
          ],
          [ 
           'name.required'             =>    'Product Name Required',
           'price.required'            =>    'Price Required',
           'quantity.required'         =>    'Quantity Required',
           'amount.required'           =>    'Amount Required',
   
          ]);
   

        $sales->update(Request::all());
        toastr()->success('Sales has been updated successfully!');
        return redirect()->route('sales.index');
    }

   
    public function delete($id)
    {
        $sales = Sales::find($id);
        $sales->delete();
        toastr()->success('Sales has been deleted successfully!');
        return redirect()->route('sales.index'); 
    }
}
