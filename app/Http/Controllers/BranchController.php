<?php

namespace App\Http\Controllers;

use App\Branch;
use Request;
use Validator;

class BranchController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index()
    {
        $branches = Branch::paginate(10);
        return view('branches.index',compact('branches'));
    }

    public function create()
    {
        return view('branches.create');
    }

    public function store()
    {
       $validator = Validator::make(Request::all(), [
         'name'                =>    'required',
         'codes'               =>    'required',
       ],
       [ 
        'name.required'              =>    'Branch Name Required',
        'codes.required'             =>    'Branch Codes Required',

       ]);

      if ($validator->fails()) {
          return redirect()->back()
           ->withErrors($validator)
           ->withInput();
      }

       Branch::create(Request::all());
       toastr()->success('Data has been saved successfully!');
       return redirect()->back();
    }

    public function edit($id)
    {
        $branch = Branch::find($id);
        return view('branches.edit',compact('branch'));
       
    }

    public function update($id)
    {
        $branch = Branch::find($id);
        $validator = Validator::make(Request::all(), [
            'name'                       =>    'required',
            'codes'                      =>    'required'
          ],
          [ 
           'name.required'              =>    'Branch Name Required',
           'codes.required'             =>    'Branch Codes Required',
          ]);
   
        if ($validator->fails()) {
             return redirect()->back()
              ->withErrors($validator)
              ->withInput();

        }

        $branch->update(Request::all());
        toastr()->success('Data has been updated successfully!');
        return redirect()->route('branches.index');
        
    }

    public function delete($id)
    {
        $branch = Branch::find($id);
        $branch->delete();
        toastr()->success('Data has been deleted successfully!');
        return redirect()->route('branches.index'); 
    }
}
