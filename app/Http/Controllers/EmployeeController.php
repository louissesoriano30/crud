<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Employee;
use Request;
use Validator;

class EmployeeController extends Controller
{

    public function index()
    {
        $employees= Employee::with('branch')->paginate(5);
        return view('employees.index',compact('employees'));
    }

    public function create()
    {
        $branches = Branch::pluck('name','id');
        return view('employees.create',compact('branches'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make(Request::all(), [
            'name'                       =>    'required|unique:employees',
            'branch_id'                  =>    'required',
            'type'                      =>    'required',
          ],
          [ 
           'name.required'              =>    'Employee Name Required',
           'branch_id.required'         =>    'Please Select Branch',
           'type.required'              =>    'Employee Type Required',
   
          ]);
   
         if ($validator->fails()) {
             return redirect()->back()
              ->withErrors($validator)
              ->withInput();
         }

         Employee::create(Request::all());
         toastr()->success('Employee has been saved successfully!');
         return redirect()->back();
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $employee = Employee::find($id);
        $branches = Branch::pluck('name','id');
        return view('employees.edit',compact('employee','branches'));
    }

    public function update($id)
    {
        $employee = Employee::find($id);
        $validator = Validator::make(Request::all(), [
            'name'                     =>    "required|unique:employees,$employee->id,id",
            'branch'                   =>    'required',
            'type'                     =>    'required',
          ],
          [ 
           'name.required'              =>    'Employee Name Required',
           'branch.required'            =>    'Employee Branch Required',
           'type.required'              =>    'Employee Type Required',
   
          ]);
   
         if ($validator->fails()) {
             return redirect()->back()
              ->withErrors($validator)
              ->withInput();
         }

         $employee->update(Request::all());
         toastr()->success('Employee has been updated successfully!');
         return redirect()->back();
    }

    public function delete($id)
    {
        $employee = Employee::find($id);
        $employee->delete();
        toastr()->success('Employee has been deleted successfully!');
        return redirect()->back();
    }
}
