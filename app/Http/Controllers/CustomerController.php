<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Customer;
use Request;
use Validator;

class CustomerController extends Controller
{

    public function index()
    {
        $customers= Customer::with('branch')->paginate(5);
        return view('customers.index',compact('customers'));
    }

    public function create()
    {
        $branches = Branch::pluck('name','id');
        return view('customers.create',compact('branches'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make(Request::all(), [
            'name'                       =>    'required|unique:customers',
            'branch_id'                   =>    'required',
            'agent'                      =>    'required',
          ],
          [ 
           'name.required'              =>    'Customer Name Required',
           'branch_id.required'         =>    'Please Select Branch',
           'agent.required'             =>    'Customer Agent Required',
   
          ]);
   
         if ($validator->fails()) {
             return redirect()->back()
              ->withErrors($validator)
              ->withInput();
         }

        Customer::create(Request::all());
         toastr()->success('Customer has been saved successfully!');
         return redirect()->back();
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $customer = Customer::find($id);
        return view('customers.edit',compact('customer'));
    }

    public function update($id)
    {
        $customer = Customer::find($id);
        $validator = Validator::make(Request::all(), [
            'name'                       =>    "required|unique:customers,$customer->id,id",
            'branch'                  =>    'required',
            'agent'                      =>    'required',
          ],
          [ 
           'name.required'              =>    'Customer Name Required',
           'branch.required'         =>    'Customer Branch Required',
           'agent.required'             =>    'Customer Agent Required',
   
          ]);
   
         if ($validator->fails()) {
             return redirect()->back()
              ->withErrors($validator)
              ->withInput();
         }

         $customer->update(Request::all());
         toastr()->success('Customer has been updated successfully!');
         return redirect()->back();
    }

    public function delete($id)
    {
        $customer = Customer::find($id);
        $customer->delete();
        toastr()->success('Customer has been deleted successfully!');
        return redirect()->back();
    }
}
