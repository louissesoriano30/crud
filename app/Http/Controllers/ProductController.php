<?php

namespace App\Http\Controllers;

use App\Product;
use Request;

class ProductController extends Controller
{
    
    public function index()
    {
        $products = Product::paginate(10);
        return view('products.index',compact('products'));
    }

    
    public function create()
    {
        return view('products.create');
    }

    
    public function store()
    {
        $validator = Validator::make(Request::all(), [
            'name'                       =>    'required',
            'codes'                      =>    'required',
            'date arrival'               =>    'required',
            'expiry date'                =>    'required',
            'selling price'              =>    'required',
            'original price'             =>    'required',
            'quantity'                   =>    'required',
          ],
          [ 
           'name.required'                =>    'Product Name Required',
           'codes.required'               =>    'Product Codes Required',
           'date arrival.required'        =>    'Date Arrival Required',
           'expiry date.required'         =>    'Expiry Date Required',
           'selling price.required'       =>    'Selling Price Required',
           'original price.required'      =>    'Original Price Required',
           'quantity.required'            =>    'Quantity Required',
   
          ]);
   
         if ($validator->fails()) {
             return redirect()->back()
              ->withErrors($validator)
              ->withInput();
         }
   
         Product::create(Request::all());
          toastr()->success('Product has been saved successfully!');
          return redirect()->back();
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        $product = Product::find($id);
        return view('products.edit',compact('product'));
    }

    
    public function update($id)
    {
        $validator = Validator::make(Request::all(), [
            'name'                       =>    'required',
            'codes'                      =>    'required',
            'date arrival'               =>    'required',
            'expiry date'                =>    'required',
            'selling price'              =>    'required',
            'original price'             =>    'required',
            'quantity'                   =>    'required',
          ],
          [ 
           'name.required'                =>    'Product Name Required',
           'codes.required'               =>    'Product Codes Required',
           'date arrival.required'        =>    'Date Arrival Required',
           'expiry date.required'         =>    'Expiry Date Required',
           'selling price.required'       =>    'Selling Price Required',
           'original price.required'      =>    'Original Price Required',
           'quantity.required'            =>    'Quantity Required',
   
          ]);

        $product->update(Request::all());
        toastr()->success('Product has been updated successfully!');
        return redirect()->route('products.index');
        
    }

   
    public function delete($id)
    {
        $product = Product::find($id);
        $product->delete();
        toastr()->success('Product has been deleted successfully!');
        return redirect()->route('products.index'); 
    }
}
